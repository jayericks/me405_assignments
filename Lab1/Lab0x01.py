""" @file       Lab0x01.py
    @brief      Full Vendotron Program
    @details    This program is the software backbone for
                Vendotron, an automated vending machine.
                The user will interact with Vendotron
                through the command line, where they may
                increase their monetary balance, select
                a beverage, or request their balance to be
                returned to them.
    @author     Jonny Erickson
    @date       1/19/2021
"""
import keyboard

# Initialize Variables 

## @brief State Control Variable
# @details Variable controls the state of the system
state = 0

## @brief Holds the monetary balance of the user
# @details As the user inputs more money, the value is
#          updated and held in this variable
balance = 0

last_key = ''

def kb_cb(key):
    '''
    @brief    Helper Function for Identifying Keys Pressed
    @param key Key pressed on Keyboard, Entered into Console
    '''
    global last_key
    last_key = key.name

def getChange(price, payment):
    '''
    @brief      Computes Needed Change for Given Price and Payment
    @details    Given a set of cash and coin values, as well as the
                price of the item being purchased, the getChange() 
                function finds the minimum number of denominations 
                required to return exact change.
    @param price Positive Float/Integer
    @param payment Tuple of length 8 
    '''
    
    Denoms = ['Pennies','Nickels','Dimes', 'Quarters', 'Ones', 'Fives', 'Tens', 'Twenties']
    Change = [0,0,0,0,0,0,0,0]
    ChangeBack = ['','','','','','','','',]
    

    Result = round(payment - price, 2)
    

    
    if (payment < price):
        print('Insufficient Payment!' '\n')
        return None


    while Result > 0:

        if Result > 20:
            Result = round(Result - 20,2)
            Change[-1] += 1
            continue

        if Result >= 10:
            Result = round(Result - 10,2)
            Change[-2] += 1
            continue

        if Result >= 5:
            Result = round(Result - 5,2)
            Change[-3] += 1
            continue

        if Result >= 1:
            Result = round(Result - 1,2)
            Change[-4] += 1
            continue

        if Result >= 0.25:
            Result = round(Result - 0.25,2)
            Change[-5] += 1
            continue

        if Result >= 0.1:
            Result = round(Result - 0.1,2)
            Change[-6] += 1
            continue

        if Result >= 0.05:
            Result = round(Result - 0.05,2)
            Change[-7] += 1
            continue

        if Result >= 0.01:
            Result = round(Result - 0.01,2)
            Change[-8] += 1
            continue 
    for i in range(len(Change)):
        if Change[i] != 0:
            ChangeBack[i] = f"{Change[i]} {Denoms[i]}"
        else:
            continue
        
    return ChangeBack

def printWelcome():
    '''
    @brief      Prints welcome message
    @details    Explains to user how to use Vendotron,
                as well as display prices
    '''
    print("------- Vendotron -------" '\n'
          "                         " '\n'
          "         Payment         " '\n'
          " 0 = Penny    1 = Nickel " '\n'
          " 2 = Dime     3 = Quarter" '\n'
          " 4 = One      5 = Five   " '\n'
          " 6 = Ten      7 = Twenty " '\n'
          "                         " '\n'
          "        Beverages        " '\n'
          " c = Cuke ( $ 2.25 )     " '\n'
          " p = Popsy ( $ 2.50 )    " '\n'
          " s = Spryte ( $ 2.00 )   " '\n'
          " d = Dr. Popper ( $ 4.50)" '\n'
          "                         ")
          
          
# Keyboard Module Responding to input money   
# 0->Penny, 1->Nickle, 2->Dime,3->Quarter
# 4->One, 5->Five, 6->Ten, 7->Twenty 
keyboard.on_release_key("0", callback=kb_cb)
keyboard.on_release_key("1", callback=kb_cb)
keyboard.on_release_key("2", callback=kb_cb)
keyboard.on_release_key("3", callback=kb_cb)
keyboard.on_release_key("4", callback=kb_cb)
keyboard.on_release_key("5", callback=kb_cb)
keyboard.on_release_key("6", callback=kb_cb)
keyboard.on_release_key("7", callback=kb_cb)

# Keyboard Module Responding to Beverage Selection
# c->Cuke, p->Popsy, s->Spryte, d->Dr. Popper
keyboard.on_release_key("c", callback=kb_cb)
keyboard.on_release_key("p", callback=kb_cb)
keyboard.on_release_key("s", callback=kb_cb)
keyboard.on_release_key("d", callback=kb_cb)

# Keyboard Module Responding to Money Eject
# e->Eject
keyboard.on_release_key("e", callback=kb_cb)

# Define payment dictionary
PaymentConversion = {
    "0":0.01,
    "1":0.05,
    "2":0.10,
    "3":0.25,
    "4":1,
    "5":5,
    "6":10,
    "7":20}
# Define Price Dictionary
PriceConversion = {
    "c":2.25,
    "p":2.50,
    "s":2.00,
    "d":4.50}
# Define Name Dictionary
NameConversion = {
    "c":"Cuke",
    "p":"Popsy",
    "s":"Spryte",
    "d":"Dr. Popper"}

while True:
    
    if state == 0:
        
        printWelcome()
        balance = 0
        state = 1
    
    
    if state == 1:  
        
        if last_key == '':
            continue
        
        if last_key in "01234567":
            balance += PaymentConversion[last_key]
            last_key = ''
            continue
        
        if last_key in "cpsd":
            state = 2
        
        if last_key == "e":
            state = 3
    
    
    if state == 2:
        
        ChangeBack = getChange(PriceConversion[last_key], balance)
        if ChangeBack == None:
            state == 1
            
        else:
            balance = round( balance- PriceConversion[last_key], 2)
            print(' ' '\n')
            print(f"Dispensing: {NameConversion[last_key]}" '\n')
            print(f"Remaining Balance: ${balance}" '\n')
            if balance == 0:
                print('Thank you for using Vendotron!')
                print('\n')

        last_key = ''
        state = 1
            
    
    if state == 3:
        
        ChangeBack = getChange(0, balance)

        print("Returning:")
        for denom in ChangeBack:
            if denom != '':
                print(denom)
        
        print('\n')
        print('Thank you for using Vendotron!')
        print('\n')
        
        last_key = ''
        state = 0
        
            
            
        
        
        
        
        
        
        
        
        
