'''
@file       main.py
@brief      Testing module for touch-screen driver
@details    Hardware specifics for on-board the Nucelo. Reminder that 
            Professor Ridgely has worked to fix this Doxygen bug and still
            is not working for some parts of the file.
@author     Jonny Erickson
@date       3/4/2021
'''
from pyb import Pin
import utime
from ResistiveTouch import ResistiveTouch
## @brief Assign X-Measurement Pin Low 
x_lo = Pin.board.PA1
## @brief Assign X-Measurement Pin Hig
x_hi = Pin.board.PA7
## @brief Assign Y-Measurement Pin Low
y_lo = Pin.board.PA0
## @brief Assign Y-Measurement Pin High
y_hi = Pin.board.PA6

## @brief Declare Active Region Length on X-axis
X = 0.178
## @brief Declare Active Region Length on Y-axis
Y = 0.103

## @brief Create ResistiveTouch Object with desired parameters
resTouch = ResistiveTouch(x_lo, x_hi, y_lo, y_hi, X, Y)
utime.sleep(5)

while True:
    utime.sleep(0.5)
    st = utime.ticks_us()
    print(str(resTouch.xyzRead()))
    print(str(utime.ticks_diff(utime.ticks_us(), st)))
    
