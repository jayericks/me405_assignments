'''
@file       ResistiveTouch.py
@brief      Touch-screen driver for reading x,y,and z values
@details    Class definition that encompasses functionality for reading
            if there is something touching the screen (z) and then the 
            position of that object on the x and y axes. 
@author     Jonny Erickson
@date       3/4/2021
'''
import pyb

class ResistiveTouch:
    '''
    @brief Reading the X,Y,and Z positions on touch-screen
    @details Class used to initialize the reading for the touch-screen
    with parameters of the screen (measurable length and width) and the
    desired reading pins. ResistiveTouch also contains the methods to read 
    X position, Y position, Z position, or all three at once. The Z measurement
    simply returns a boolean True if an object is in contact with the screen.
    '''
    def __init__(self, Pin1, Pin2, Pin3, Pin4, X, Y):
        '''
        @brief Initialization for pins and screen parameters
        @param Pin1 Pin for X measurement ground
        @param Pin2 Pin for X measurement power
        @param Pin3 Pin for Y measurement ground
        @param Pin4 Pin for Y measurement power
        @param X Length of active region for touch screen on X-axis
        @param Y Length of active region for touch screen on Y-axis
        '''
        ## Assign X-Measurement Pin Low 
        self.x_lo = Pin1
        ## Assign X-Measurement Pin High
        self.x_hi = Pin2
        ## Assign Y-Measurement Pin Low
        self.y_lo = Pin3
        ## Assign Y-Measurement Pin High
        self.y_hi = Pin4
        ## X-axis Length for Active Region
        self.X = X
        ## Y-axis Length for Active Region
        self.Y = Y
        ## ADC Conversion for X measurement
        self.x_adc = X/3560
        ## ADC Conversion for Y measurement
        self.y_adc= Y/3220
        
    def xRead(self):
        '''
        @brief xRead Method determines current touch position along x-axis
        @details By powering the X-axis resistive circuit, we can then read
        off of the Low Y pin for position. 
        @return X position on the touch-screen
        '''
        self.x_lo.init(mode = pyb.Pin.OUT_PP, value = 0)
        self.x_hi.init(mode = pyb.Pin.OUT_PP, value = 1)
        self.y_lo.init(mode = pyb.Pin.IN)
        self.y_hi.init(mode = pyb.Pin.IN)
        
        self.y_lo.init(mode = pyb.Pin.ANALOG)
        return(self.x_adc * (2009 - pyb.ADC(self.y_lo).read()))
    
    def yRead(self):
        '''
        @brief yRead Method determines current touch position along y-axis
        @details By powering the Y-axis resistive circuit, we can then read
        off of the Low X pin for position. 
        @return Y position on the touch-screen
        '''
        self.y_lo.init(mode = pyb.Pin.OUT_PP, value = 0)
        self.y_hi.init(mode = pyb.Pin.OUT_PP, value = 1)
        self.x_lo.init(mode = pyb.Pin.IN)
        self.x_hi.init(mode = pyb.Pin.IN)
        
        self.x_lo.init(mode = pyb.Pin.ANALOG)
        return(self.y_adc * (1945 - pyb.ADC(self.x_lo).read()))
    
    def zRead(self):
        '''
        @brief zRead Method determines if an object is touching the screen
        @details By setting the Y-Power pin to high, and the X-Ground pin 
        to low, we can then read across the entire screen to see if any
        object is present on it.
        @return X position on the touch-screen
        '''
        self.x_lo.init(mode = pyb.Pin.OUT_PP, value = 0)
        self.x_hi.init(mode = pyb.Pin.IN)
        self.y_lo.init(mode = pyb.Pin.IN)
        self.y_hi.init(mode = pyb.Pin.OUT_PP, value = 1)
        
        self.y_lo.init(mode = pyb.Pin.ANALOG)
        return((pyb.ADC(self.y_lo).read() < 4000))
        
        
    
    def xyzRead(self):
        '''
        @brief xyzRead Method determines all three positional values
        @details xyzRead basically runs through the xRead, yRead and zRead 
        methods, however, does so in an order where the least amount of 
        pin changes are required between steps
        @return Tuple of x position, y position, boolean z
        '''
        self.x_lo.init(mode = pyb.Pin.OUT_PP, value = 0)
        self.x_hi.init(mode = pyb.Pin.OUT_PP, value = 1)
        self.y_lo.init(mode = pyb.Pin.IN)
        self.y_hi.init(mode = pyb.Pin.IN)
        
        self.y_lo.init(mode = pyb.Pin.ANALOG)
        cX = self.x_adc * (2009 - pyb.ADC(self.y_lo).read())
        
        self.x_hi.init(mode = pyb.Pin.IN)
        self.y_hi.init(mode = pyb.Pin.OUT_PP, value = 1)
        cZ = (pyb.ADC(self.y_lo).read() < 4000)
        
        self.y_lo.init(mode = pyb.Pin.OUT_PP, value = 0)
        self.y_hi.init(mode = pyb.Pin.OUT_PP, value = 1)
        self.x_lo.init(mode = pyb.Pin.IN)
        self.x_hi.init(mode = pyb.Pin.IN)
        
        self.x_lo.init(mode = pyb.Pin.ANALOG)
        return((cX, self.y_adc * (1945 - pyb.ADC(self.x_lo).read()), cZ))