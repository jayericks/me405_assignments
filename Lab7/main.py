'''
@file       main.py
@brief      Testing module for touch-screen driver
@details    Hardware specifics for on-board the Nucelo. Reminder that 
            Professor Ridgely has worked to fix this Doxygen bug and still
            is not working for some parts of the file.
@author     Jonny Erickson
@date       3/4/2021
'''
from pyb import Pin
import utime
from ResistiveTouch import ResistiveTouch

x_lo = Pin.board.PA1
x_hi = Pin.board.PA7

y_lo = Pin.board.PA0
y_hi = Pin.board.PA6

X = 0.178
Y = 0.103

resTouch = ResistiveTouch(x_lo, x_hi, y_lo, y_hi, X, Y)
utime.sleep(5)

while True:
    utime.sleep(0.5)
    st = utime.ticks_us()
    print(str(resTouch.xyzRead()))
    print(str(utime.ticks_diff(utime.ticks_us(), st)))
    
