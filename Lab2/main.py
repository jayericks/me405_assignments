
'''
@file main.py
@brief Executing the Reaction Time Program on the Nucleo
@author Jonny Erickson
@date 1/24/2021
'''
import pyb
import utime
import urandom

timer = pyb.Timer(2, 
                  prescaler=79, 
                  period=1000000)

blinko = pyb.Pin(pyb.Pin.board.PA5, 
                 mode=pyb.Pin.OUT_PP)

def count_isr(what_timer):
    '''
    @brief      Interrupt Callback Function
    @details    This function is called when the button 
                is pressed on the Nucleo board. The length
                of time between the light blinking and the
                user pressing the button is recorded.
    @param what_timer Dummy variable
    '''
    
    global reaction_time
    global attempts
    reaction_time = timer.counter()
    attempts += 1
    
extint = pyb.ExtInt(pyb.Pin.board.PC13, 
                    pyb.ExtInt.IRQ_FALLING,
                    pyb.Pin.PULL_UP, 
                    count_isr)

##      @brief Sum of all reaction times prior to keyboard interrupt 
reaction_time_sum = 0
##      @brief Number of all attempts prior to keyboard interrupt
attempts = 0
##      @brief Reaction time of current attempt cycle
reaction_time = 0
##      @brief Random delay between next attempt cycle
delay = 0

try:
    while(True):
        # Disable Interrupt
        pyb.ExtInt.disable()
        # Wait for Random Delay
        delay = urandom.randint(1000,3000)
        utime.sleep_ms(delay)
        # Enable Interrupt
        pyb.ExtInt.enable()
        # LED On
        blinko.high()
        # Begin counter
        timer.counter(0)
        # Hold LED On
        utime.sleep_ms(1000)
        # LED Off
        blinko.low()
        # Add current reaction time to sum of all reaction times
        reaction_time_sum += reaction_time

except KeyboardInterrupt:
    if attempts == 0:
        print('Try Again')
    else:
        # Calculate Average Reaction Time
        average_reaction = (reaction_time_sum / 1000000) / attempts
        print("Average Reaction Time = {}".format(average_reaction))
        
        
        
