'''
@file ui.py
@brief UI to allow the user to begin program on Nucleo
@details The user-interface will first request that the user presses
          the letter G to begin the program. Then, the Nucleo will wait
          until the button is pressed, where the voltage will be measured
          by the ADC on Pin0. This data is then saved to a CSV and plotted.
@author Jonny Erickson
@date 2/4/2021
'''
import serial
import numpy as np
import matplotlib.pyplot as plt
import csv

## @brief Declaring serial port
# @details Assigning serial port across COM6 at baud rate of 115273
ser = serial.Serial(port='COM6', baudrate=115273, timeout=1)

def sendChar():
    ##
    # @brief Reqests and sends a value over the serial port
    # @details Reqests an input the terminal. Once an input is given 
    #           by the user, the value is written to the Nucleo, and 
    #           the function waits for an echoed response and returns it.

    res = input('Please press "G" to start')
    ser.write(str(res).encode('ascii'))
    echo = ser.readline().decode('ascii')
    return echo

## @brief State Control Variable
# @details Variable controls the state of the system
state = 1
## @brief Initialize array of voltage values
# @details This array will hold the voltage values taken from the ADC
voltVals = np.array([])
## @brief Establish ADC conversion 
# @details This formula converts raw digital values to the analog voltage
adcConvert = round(3.3/4095, 2)

while True:
    
    # Wait State
    if state == 1:
        echo = sendChar()
        if echo == 'G':
            state = 2
        else:
            print('The letter "G" was not pressed. Try again.')
        
    # Data Collection State            
    elif state == 2:
        while ser.in_waiting != 0:
            # Format for ser.readline() taken from stackoverflow
            # https://stackoverflow.com/questions/16250358/understanding-characters-received-from-arduino
            val = float(ser.readline().strip())
            voltVals = np.append(voltVals, val*adcConvert)
        # Establish time
        timeVals = np.linspace(1,len(voltVals))/1024
        
        # Write csv file
        with open('voltageData.csv', 'w', newline='') as file:
            writer = csv.writer(file)
            for volt, time in zip(voltVals, timeVals):
                writer.writerow([time, volt])
        
        # Plot the data points
        figure = plt.figure()
        figure.plot(timeVals, voltVals)
        figure.set_ylabel(' Analog Voltage (V)')
        figure.set_xlabel('Time (s)')
        figure.set_title('ADC Results from Button Press, Read at 1024 Hz')
        plt.show()
        
        # Return to wait state
        state = 1
        
        
            
            
            
    