
'''
@file lab3main.py
@brief On-board program operating the ADC and Interrupts
@details This program will allow nucleo to record data
and transmit it to the computer, given that the user
has sent a 'G' to the Nucelo, and then pressed a button
@author Jonny Erickson
@date 2/4/2021
'''

import pyb
import array
from pyb import UART


# Adapted from micropython documentation (class ADC)
# http://docs.micropython.org/en/v1.9.3/pyboard/library/pyb.ADC.html

## @brief Assign ADC to pin 0
# @details Pin 0 will now read analog voltage via ADC when called
adcPin = pyb.ADC(pyb.Pin.board.PA0) 
## @brief Create buffer
# @details Buffer with 200 hundred indices
buf = array.array('H', (0 for index in range(200)))
## @brief Create timer
# @details Timer running at 1024 Hz
tim = pyb.Timer(1, freq=1024)
## @brief Establish UART
# @details Establish UART connection
uartLine = UART(2)

# defining interrupt service routines for ADC

def isrADC(what_timer):
    ##
    # @brief Interrupt service routinee callback function
    # @details Raises the collect flag to operate in state 2
    # @param what_timer Dummy variable for interpreter
    adcPin.read_timed(buf, tim)
    global collect
    collect = 1
    
    
## @brief Assign interrupt to pin 13
# @details Interrupt pin 13 with callback function to isrADC()
extInt = pyb.ExtInt(pyb.Pin.board.PC13,
                    pyb.ExtInt.IRQ_FALLING,
                    pyb.Pin.PULL_UP,
                    isrADC)
## @brief State Control Variable
# @details Variable controls the state of the system
state = 1

while True:
    
    # Wait State
    if state == 1:
        # Check for any character waiting to be read
        if uartLine.any() != 0:
            # Read character
            init = chr(uartLine.readchar())
            # Check if the character is the letter 'G'
            if chr(init) == chr('G'):
                state = 2
                
    # Data Collection State            
    elif state == 2:
        if collect == 1:
            # Reinitialize to Wait State
            state = 1
            # Lower isrADC flag
            collect = 0
            # Write each value in the buffer over uartLine
            for value in buf:
                uartLine.write(value)
                uartLine.write(' \n')
        
        
        
        
            
        
        

        


