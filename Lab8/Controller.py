"""
@file       Controller.py
@brief      This file contains the functionality to control the system
@details    This program takes in the current measurements for the state variables and uses the K matrix gain values
            to implement feedback control. The K values require scaling so scaling is done withing the compute()
            method.

@author     Matt Murray
@date       3/17/2021
"""

class Controller:
    '''
    @brief A class that returns duty cycles to control the system

    @details This class takes in the current measurements for the state variables and uses the K matrix gain values
    to implement feedback control. The K values require scaling so scaling is done withing the compute()
    method.
    '''

    def __init__(self):
        '''
        @brief Constructor to assign K values
        '''

        # final gains for ball control

        ## variable for K1 gain
        self.K1 = -.4164

        ## variable for K2 gain
        self.K2 = .2584

        ## variable for K2 gain
        self.K3 = .0390

        ## variable for K2 gain
        self.K4 = .00147

        # table level without ball
        # self.K1 = 0
        # self.K2 = .3584
        # self.K3 = 0
        # self.K4 = .00147

    def compute(self, pos, posDot, theta, thetaDot):
        """
        @brief class method to compute duty cycle for one motor
        @details this method calculates the torque needed to correct the system on one axis. this torque is equal
        to evaluating the matirx product of -K*X where X is is the state variable matrix. the needed torque is then
        converted to duty cycle. This scalar conversion is multiplied by another scalar to compensate for the low
        values of our K matrix.
        @return the method returns a duty cycle
        """
        ## input assignment for pos
        self.pos = pos

        ## input assingment for posDot
        self.posDot = posDot

        ## input assignment for theta
        self.theta = theta

        ## input assignment for thetaDot
        self.thetaDot = thetaDot

        # compute torque needed
        T = -1 * (self.K1 * self.pos) - (self.K2 * self.theta) - (self.K3 * self.posDot) - (self.K4 * self.thetaDot)
        # find duty cycle needed
        L = int((T * 2.21 / (12 * .00138)) * 7)
        return (L)