"""
@file       main.py
@brief      This file contains the functionality of the ball ballancing table controller
@details    this program imports all needed modules such as the touch driver, motor driver and controller classes
            to control a ball on the table. The program utilizes a LQR feedback controller to move the motors
            and level the platform in such a way that the ball is centered.

@author     Matt Murray
@date       3/17/2021
"""

import pyb
from pyb import Pin, I2C
import utime
from Encoder import Encoder
from Touch_Driver import Touch_Driver
from MotorDriver import MotorDriver
from Controller import Controller
import math

#####################################
# initialize Encoder
#####################################

P1y = pyb.Pin.board.PB6  # encoder E1 channel 1
P2y = pyb.Pin.board.PB7  # encoder E1 channel 2
timery = 4

P1x = pyb.Pin.board.PC6  # encoder E2 channel 1
P2x = pyb.Pin.board.PC7  # encoder E2 channel 2
timerx = 8

encx = Encoder(P1x, P2x, timerx)
ency = Encoder(P1y, P2y, timery)


## Contains a Touch Driver object that drives data aquisition form the touch screen
td = Touch_Driver(Pin.board.PA1, Pin.board.PA7, Pin.board.PA0, Pin.board.PA6, .176, .103)

## Contains a Motor Driver object for the motor on the Y axis
motoY = MotorDriver(pyb.Pin.cpu.A15, pyb.Pin.cpu.B4, pyb.Pin.cpu.B5, 3, 1, 2)

## Contains a Motor Driver object for the motor on the X axis
motoX = MotorDriver(pyb.Pin.cpu.A15, pyb.Pin.cpu.B0, pyb.Pin.cpu.B1, 3, 3, 4)

## Contains motor controller object for motorX
ssX = Controller()

## Contains motor controller object for motorY
ssY = Controller()

## global flag to disable program
fault = False

def nfault(place):
    """
    @brief Interupt for disabling the motors and clears motor information
    """
    global fault
    fault = True
    motoX.set_duty(0)
    motoY.set_duty(0)

def reset(place):
    """
    @brief Interupt for clearing the nfault pin
    """
    global fault
    fault = False

## ISR for when N-fault is triggered on PB2
extint1 = pyb.ExtInt(pyb.Pin.board.PB2, pyb.ExtInt.IRQ_FALLING, pyb.Pin.PULL_UP, nfault)

## ISR to clear N-fault flag
extint2 = pyb.ExtInt(pyb.Pin.board.PC13, pyb.ExtInt.IRQ_FALLING, pyb.Pin.PULL_UP, self.reboot)


# enable both motors and avoid ISR
pyb.ExtInt.disable(extint1)
motoX.enable()
motoY.enable()
pyb.ExtInt.enable(extint1)

def calcAngle():
    """
    @brief function to measure angles and angular velocities of the table
    @details this fucntion records the time to take two measurements on one axis and calculates the angular
    velocity of one motor. it does this for both motors to get a representation of the table conditions.
    the function also filters out upper outliers from noise on the system that come in as incorect velocities
    @return returns a tuple of (Ax,Ay,AVx,AVy)
    """

    encx.update()
    #measure time between two measurements
    start_time = utime.ticks_us()
    Ax1 = (math.pi/180)*encx.getPosition()
    utime.sleep_us(10)
    encx.update()
    Ax2 = (math.pi/180)*encx.getPosition()
    end_time = utime.ticks_us()

    #calculate AVx
    AVx = (Ax2-Ax1)/(utime.ticks_diff(end_time, start_time)/100000)


    ency.update()
    #measure time between two measurements
    start_time = utime.ticks_us()
    Ay1 = (math.pi/180)*ency.getPosition()
    utime.sleep_us(10)
    ency.update()
    Ay2 = (math.pi/180)*ency.getPosition()
    end_time = utime.ticks_us()

    #calculate AVy
    AVy = (Ay2-Ay1)/(utime.ticks_diff(end_time, start_time)/100000)

    return((Ax2,Ay2,AVx,AVy))

def calcVelocity():
    """
    @brief function to measure positions and calculate ball velocities
    @details this fucntion records the time to take two measurements on one axis and calculates the velocity.
    it does this for both axies and also scans if a touch is made. the positions returned are the latter
    measurements to ensure a measurement close to real time
    @return returns a tuple of (x,y,Vx,Vy,Z)
    """

    #Pull X and calculate Vx
    start_time = utime.ticks_us()
    x1 = td.Xscan()
    utime.sleep_us(10)
    x2 = td.Xscan()
    end_time = utime.ticks_us()
    Vx = (x2-x1)/(utime.ticks_diff(end_time, start_time)/100000)

    #filter outliers
    if abs(Vx) > 1:
        Vx = Vx/100
        x2 = 0


    #Pull Y and calculate Vy
    start_time = utime.ticks_us()
    y1 = td.Yscan()
    utime.sleep_us(10)
    y2 = td.Yscan()
    end_time = utime.ticks_us()

    Vy = (y2-y1)/(utime.ticks_diff(end_time, start_time)/100000)

    #filter outliers
    if abs(Vy) > 1:
        Vy = Vy/100


    #Pull X boolean
    Z = td.Zscan()

    return((x2,y2,Vx,Vy,Z))

## a USB object for detecting characters on the buss
vcp = pyb.USB_VCP()

while not vcp.any():

    #calculate angle
    angle_data = calcAngle()
    Ax = angle_data[0]
    Ay = angle_data[1]
    AVx = angle_data[2]
    AVy = angle_data[3]

    #update calcs for position
    touch_data = calcVelocity()
    x = touch_data[0]
    y = touch_data[1]
    Vx = touch_data[2]
    Vy = touch_data[3]
    Z = touch_data[4]

    if Z and not fault:
        #calculate duty cycles for both motors
        dutyX = ssX.compute(x, Vx, Ax, AVx)
        dutyY = ssY.compute(y, Vy, Ay, AVy)

        #set duty cycles with a scaled value
        motoX.set_duty(dutyX*1.3)
        motoY.set_duty(dutyY*1.2)

        #print metrics for system
        print("settingDuty", dutyX, dutyY)
        print("x  "+ repr(x) +"  Vx  "+ repr(Vx) +"     Ax  "+ repr(Ax) +"  AVx  " + repr(AVx))
        print("y  " + repr(y) + "  Vy  " + repr(Vy) + "     Ay  " + repr(Ay) + "  AVy  " + repr(AVy))
        print('\n')




print("disable motors")
motoX.disable()


