"""
@file MotorDriver.py
@brief A file that contains a motor driver class
@details This file implements a motor driver class that is able to use multiple instances at the same time
The constructor initializes the pins for control as well as uses one timer to initialize two timer channel
objects. The enable and disable methods toggle one pin that controls whether or not the motors are operating.
The set duty method controls the speed and direction of motion. +duty == CCW and -duty == CW motion
@author Jonny Erickson
"""

import pyb


class MotorDriver:
    """
    @brief A motor driver class
    @details This file implements a motor driver class that is able to use multiple instances at the same time
    The constructor initializes the pins for control as well as uses one timer to initialize two timer channel
    objects. The enable and disable methods toggle one pin that controls whether or not the motors are operating.
    The set duty method controls the speed and direction of motion. +duty == CCW and -duty == CW motion
    """

    def __init__(self, nSLEEP_pin, IN1_pin, IN2_pin, timer, channel1, channel2):
        """
        @brief constructs needed components for motor driver class
        @details constructor assigns pins for GPIO as well as for control of the motors. Then using
        the timer and the channel inputs it creates two timer channel objects used to manipulate the motors
        @param nSLEEP_pin A pyb.Pin object to use as the enable pin.
        @param IN1_pin A pyb.Pin object to use as the input to half bridge 1.
        @param IN2_pin A pyb.Pin object to use as the input to half bridge 2.
        @param timer A pyb.Timer object to use for PWM generation on IN1_pin and IN2_pin.
        @param channel1 contains channel for pin 1
        @param channel2 contains channel for pin 2
        """

        ## A sleep pin object
        self.pinSLEEP = pyb.Pin(nSLEEP_pin, pyb.Pin.OUT_PP)
        self.pinSLEEP.low()

        ## A pyb.Pin object
        self.pin1 = pyb.Pin(IN1_pin)

        ## A pyb.Pin object
        self.pin2 = pyb.Pin(IN2_pin)

        ## A timer object
        self.tim = pyb.Timer(timer, freq=20000)

        ## Timer Ch 1 object that is tied to Pin 1
        self.Timer_Ch1 = self.tim.channel(channel1, pyb.Timer.PWM, pin=self.pin1)
        self.Timer_Ch1.pulse_width_percent(0)

        ## Timer Ch 2 object that is tied to Pin 2
        self.Timer_Ch1 = self.tim.channel(channel2, pyb.Timer.PWM, pin=self.pin2)
        self.Timer_Ch2.pulse_width_percent(0)


    def enable(self):
        """
        @brief Enables the motor by setting the sleep pin high
        """
        print('Enabling Motor')
        self.pinSLEEP.high()

    def disable(self):
        """
        @brief Disables the motor by setting the sleep pin high
        """
        print('Disabling Motor')
        self.pinSLEEP.low()

    def set_duty(self, duty):
        """
        @brief This method controls the motor speed and direction
        @details motor speed as commonly known is controlled by the pulse width of the PWM signal. Our motors are
        controlled by an external chip and the location of the PWM signal controls direction. Therefore a negative
        duty cycle will turn the motor CW by applying the signal to Pin1 and CCW by applying the signal to Pin2
        @param duty duty is the specified duty cycle given in 0->100.
        """

        self.Timer_Ch2.pulse_width_percent(0)
        self.Timer_Ch1.pulse_width_percent(0)
        if duty > 100:
            self.Timer_Ch2.pulse_width_percent(100)
        elif duty < -100:
            self.Timer_Ch1.pulse_width_percent(100)
        elif duty < 0:
            duty = abs(duty)
            self.Timer_Ch1.pulse_width_percent(duty)
        elif duty > 0:
            self.Timer_Ch2.pulse_width_percent(duty)
        elif duty == 0:
            self.Timer_Ch2.pulse_width_percent(0)
            self.Timer_Ch1.pulse_width_percent(0)
        else:
            pass


if __name__ == '__main__':
    # pins for motor 1
    IN1Y_pin = pyb.Pin.cpu.B4
    IN2Y_pin = pyb.Pin.cpu.B5
    # pins for motor 2
    IN1X_pin = pyb.Pin.cpu.B0
    IN2X_pin = pyb.Pin.cpu.B1
    # sleep and timer for both motors
    sleep_pin = pyb.Pin.cpu.A15
    timer = 3

    motoY = MotorDriver(sleep_pin, IN1Y_pin, IN2Y_pin, timer, 1, 2)
    motoX = MotorDriver(sleep_pin, IN1X_pin, IN2X_pin, timer, 3, 4)

    pyb.ExtInt.disable(extint2)
    motoX.enable()
    motoY.enable()
    pyb.ExtInt.enable(extint2)
