"""
@file Encoder.py

@brief Encoder class to monitor angle of the table
@detials This class uses two pin objects and a timer object to setup the encoder
hardware on the chip. This built in functionality works in the background and this
class pulls the data from this hardware to convert to degrees deviated from the initial
operating point. the method Update() needs to be called frequently enough so that the
position of the table is known and the metrics are valid. The other methods allow the user
to pull the requested data

@author Jonny Erickson
"""

from pyb import Pin
import pyb

class Encoder:
    '''
    @brief Encoder class to monitor angle of the table
    @detials This class uses two pin objects and a timer object to setup the encoder
    hardware on the chip. This built in functionality works in the background and this
    class pulls the data from this hardware to convert to degrees deviated from the initial
    operating point. the method Update() needs to be called frequently enough so that the
    position of the table is known and the metrics are valid. The other methods allow the user
    to pull the requested data
    '''


    def __init__(self, P1, P2, TA):
        """
        @brief The constructor assigns initial values as well as the encoder timers
        @param P1 The first pin object tied to Ch1
        @param P2 The second pin object tied to Ch2
        @param TA The timer object for the encoder module.
        """
        ## timer object assignment
        self.TA = TA
        ## Pin 1 object assingment
        self.P1 = P1
        ## Pin 2 object assingment
        self.P2 = P2
        ## current position storage variable measured in degrees
        self.current_pos = 0
        ## current count storage variable measured in tics
        self.current_count = 0  # number of ticks
        ## previous position storage variable measured in degrees
        self.prev_pos = 0  # measured in deg
        ## previous position storage variable measured in tics
        self.prev_count = 0  # number of ticks
        ## delta storage variable measured in degrees
        self.delta = None

        if self.TA == 4:
            pin1 = pyb.Pin(self.P1, mode=pyb.Pin.AF_PP, af=pyb.Pin.AF2_TIM4)
            pin2 = pyb.Pin(self.P2, mode=pyb.Pin.AF_PP, af=pyb.Pin.AF2_TIM4)
            self.key = 'X'
        elif self.TA == 8:
            pin1 = pyb.Pin(self.P1, mode=pyb.Pin.AF_PP, af=pyb.Pin.AF3_TIM8)
            pin2 = pyb.Pin(self.P2, mode=pyb.Pin.AF_PP, af=pyb.Pin.AF3_TIM8)
            self.key = 'Y'
        else:
            print("Incorrect Pin Declaration!")

        self.enc_timer = pyb.Timer(self.TA, prescaler=1, period=65535) #can use timer 8 and 4
        enc_channel = self.enc_timer.channel(1, pyb.Timer.ENC_AB)

    def update(self):
        """
        @brief This method updates all the metrics of the class
        @details The method is intended to be called frequently so that the metrics
        of position and count are up to date. Underflow is protected by removing the
        max count if the number is close to the top. Because the counter does not
        increase by much this process works.
        """
        # move current metrics to prev metrics
        self.prev_pos = self.current_pos
        self.prev_count = self.current_count

        # assign new current metrics
        count = self.enc_timer.counter()
        if count < 400:
            self.current_count = count                  #remove underflow
            delta_count = self.current_count-self.prev_count  #calculate number of counts changed
            if self.key == 'Y':
                self.delta = delta_count * .07766        #convert to deg changed
            if self.key == 'X':
                self.delta = delta_count * .08333
            self.current_pos += self.delta

        else:
            self.current_count = count - 65535           #remove underflow
            delta_count = self.current_count-self.prev_count  #calculate number of counts changed
            if self.key == 'Y':
                self.delta = delta_count * .07766         #convert to deg changed
            if self.key == 'X':
                self.delta = delta_count * .08333
            self.current_pos += self.delta



    def getPosition(self):
        """
        @brief This method returns the current position in deg of the table
        @details The position is given in degrees and is the representation of one axis
        @return the table angle is returned
        """
        return(self.current_pos)

    def setPosition(self, pos):
        """
        @brief This method resets the position to a specified value
        @details The method resets the counter to the value given. It also resets the current
        and previous position and count variables to the desired reset angle
        @param pos the position in degrees of the table to be reset to
        """

        if self.key == 'Y':
            self.enc_timer.counter(pos/.07766)
            self.current_pos = pos
            self.current_count = pos / .07766
            self.prev_pos = pos
            self.prev_count = pos / .07766
        if self.key == 'X':
            self.enc_timer.counter(pos / .08333)
            self.current_pos = pos
            self.current_count = pos / .08333
            self.prev_pos = pos
            self.prev_count = pos / .08333


    def getDelta(self):
        """
        @brief This method returns the current delta
        @details The delta returned is the difference in degrees of the table
        @return the delta variable is returned
        """
        return(self.delta)


if __name__ == '__main__':
    # initialize encoder parameters
    P1y = pyb.Pin.board.PB6  # encoder E1 channel 1
    P2y = pyb.Pin.board.PB7  # encoder E1 channel 2
    timery = 4

    P1x = pyb.Pin.board.PC6  # encoder E2 channel 1
    P2x = pyb.Pin.board.PC7  # encoder E2 channel 2
    timerx = 8

    encx = Encoder(P1x, P2x, timerx)
    ency = Encoder(P1y, P2y, timery)

    while True:
        # utime.sleep(2)
        encx.update()
        ency.update()
        posx = encx.getPosition()
        posy = ency.getPosition()
        print(posx, posy)