'''
@filename mcp9808.py

@brief This is the file holding the class mcp9808 which holds methods for temperature readings 

@details This file relies on communication between the temperature sensor board
    and the nucleo to measure celcius and farenheit temperature values over a period of time.
    The methods in this class are used by the main.py file running on the nucleo to reccord and hold values 
    to eventually be plotted.
    
@author Ben Spin , Cole Andrews, Johnny Erickson
@date 2/11/2021
'''

import pyb
import utime

class mcp9808:
    '''
    @brief This class contains all the methods needed for the user to communicate with
        an mcp9808 digital temperature sensor using an I2C interface. These methods include
    '''
    
    def __init__(self, i2c, address):
        '''
        @brief Creates and mcp9808 class by 
        @param i2c is a created two-wire serial protocal (I2C) Object
        @param address is the address of the MCP9808 on the I2C bus
        '''
        # Make i2c and address variables accessible
        self.i2c= i2c
        self.address = address
          
        ## initializing the I2C bus
        self.i2c.init(pyb.I2C.MASTER, baudrate = 115200)
        
    
    
    def check(self):
        ''' 
        @brief This method verifies the sensor is attached at the given bus address
            by checking that the value in the manufacturer ID register is correct.
        '''

    
        try:
            ## Check that device at specified address is accessible
            self.i2c.is_ready(self.address)
        
            
            ## Request manufacturer ID across bus
            manufacturerID = self.i2c.mem_read(2, self.address, 0x06 )
            
        
        except:
            ## If anything above fails, report that attempt was unsuccessful
            print('Unable to access device or I.D. register')
        else:
            ## If successful, print manufacturer ID
            print( 'Manufacturer I.D.: {}'.format(manufacturerID))
        

    
    def celsius(self):
        '''
        @brief This method returns the measured temperature in degrees Celsius

        '''
        ## Raw temperature values
        rawT = self.i2c.mem_read(2, self.address, 0x05 )
        
        ## Decimal value of raw temperature reading
        decT = int.from_bytes(rawT, "big")
        
        ## Clearing Flag Bits
        decT &= 0x1FFF 
        
        ## Assesing possible negative temperatures
        if hex(decT & 0x1000) == '0x1000':
            
            decT &= 0x0FFF
            
            ## Math as referenced in the 9808 documentation
            celcius = 256 - ((decT & 0xFF00)>>8) * 16 + (decT & 0x00FF) / 16
        else:
            celcius = ((decT & 0xFF00)>>8) * 16 + (decT & 0x00FF) / 16

        return celcius
        
    
    def farenheit(self):
        '''
        @brief This method returns the measured temperature in degrees Fahrenheit
        '''
        
        celcius = self.celsius()
        farenheit = celcius * (9/5) + 32
        return farenheit
    
    
## File to run if a stand alone. Usefull for testing      
if __name__ == "__main__" :
    ## Address for destination
    ## Need to format address
    address = 0x18
    ## Create on bus 1
    ##   Bus 1: (SCL, SDA) = (X9, X10) = (PB6, PB7)
    i2c = pyb.I2C(1)
    
    ## creating a mcp9809 class
    mcp = mcp9808(i2c, address)      
    
    mcp.check()
    while True:
        print(mcp.celsius())
        print(mcp.farenheit())
        utime.sleep(1)
        
        
        
        
        
        
        
        
        