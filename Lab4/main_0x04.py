'''
@filename main_0x04.py

@brief This is the main.py file running on our nucleo for Lab 04

@details This file utilizes mcp9808.py and the mcp9808 class to record tempurature data
    every 60 seconds. Ambient tempurature is recorded within +-0.5 degrees celsisus
    using the mcp9808 sensor. Nucleo ECU core temp is also recorded. Time is recoded
    using utime and the program will stop running and store the data in a csv file
    when the user gives a keyboard interrupt
    
@author Ben Spin , Cole Andrews, Johnny Erickson
@date 2/11/2021
'''

import pyb
import array
import utime
from mcp9808 import mcp9808 


## @brief I2C Address for Slave
# @details 0x18 or (00011000)
address = 0x18
## @brief Creating an I2C bus on Channel 1
# @details
i2c = pyb.I2C(1)

## @brief Creating instance of MCP9808 Class
mcp = mcp9808(i2c, address)

## @brief Enabling ADC for measuring internal core temperature 
adc = pyb.ADCAll(12, 0x70000)

## @brief Creating empty array to hold Ambient Temps (C)
mcp_temp_C = array.array('f')
## @brief Creating empty array to hold Ambient Temps (F)
mcp_temp_F = array.array('f')
## @brief Creating empty array to hold Core Temps (C)
core_temp = array.array('f')
## @brief Creating empty array to hold Time
time = array.array('f')


# Starting Timer
start_time = utime.ticks_ms()
while True:

    try:
        ## recording data every 60 seconds
        mcp_temp_C.append(mcp.celsius())
        mcp_temp_F.append(mcp.farenheit())
        core_temp.append(adc.read_core_temp())
        time.append(utime.ticks_diff(utime.ticks_ms(), start_time )/1000)
        utime.sleep_ms(60000)

     
    
    # ctrl + c interrupt
    except KeyboardInterrupt:
        ## iterating through data and storing it in a csv file
        print('Interupt Recognized')
        with open ("data.csv", "w") as file:
            for (a,b,c,t) in zip(mcp_temp_C, mcp_temp_F, core_temp, time):
                file.write('{:}, {:}, {:}, {:}\r'.format(a,b, c, t))

        break

