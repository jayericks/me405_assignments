##  @file mainpage.py
#   Documentation for / use of mainpage.py
#
#   Detailed doc for mainpage.py
#
#   @mainpage
#
#
#   @section sec_port Introduction
#   The following outlines the work done by Jonny Erickson, as well as several 
#   partners throughout the quarter, in ME405: Mechatronics. The first few
#   sections cover weekly labs that were done to build up an understanding
#   of the various systems we would be working with. Then, you will see that
#   the remainder makes up the term project: a self-leveling table that is
#   capable of keeping a ball balanced in the center with a set of intitial 
#   conditions (hopefully). 
#
#   @page WeeklyLabsPage Weekly Lab Activities
#
#   @section sec_lab1 Lab 1: Finite State Machine
#   In this activity, we implemented a finite state machine using the diagrams
#   we developed in the lecture. The end product was Vendotron: a command line
#   interfaced vending machine, that could accept money, "dispense" different 
#   products, and return change in the end. 
#   * Source: https://bitbucket.org/jayericks/me405_assignments/src/master/Lab1/
#   * Documentation : \ref Lab0x01
#
#   @image html Vendotron.jpg "Figure 1: Vendotron Transition Diagram" width = 500cm
#
#   @section sec_lab2 Lab 2: Think Fast!
#   This acitivity got us acclimated with the Nucleo system, and allowed
#   us to try out using interrupts and different pin assignments to create a 
#   game where the user can measure their reaction time. The program randomly
#   chooses a time to turn on the light, and then records the time between
#   the light turning on and when the user presses the button on the board.
#   * Source: https://bitbucket.org/jayericks/me405_assignments/src/master/Lab2/
#   * Documentation : \ref main
#
#   @section sec_lab3 Lab 3: Pushing the Right Buttons
#   Here we were able to see how the communication protocols would work between
#   our board, the Nucleo, and our computer. The user instructs the board to begin
#   via UART, then waits for the user to press a button on board. When the button
#   is pressed, the 'step function' is recorded via the ADC and written to a .csv 
#   file. 
#   * Source: https://bitbucket.org/jayericks/me405_assignments/src/master/Lab3/
#   * User Interface : \ref ui
#   * Documentation : \ref lab3main
#
#   @section sec_lab4 Lab 4: Hot or Not?
#   Lab 4 introduced us to using offboard components, namely, the MCP9808 temperature
#   sensor. We were required to take temperature readings over the span of 8 hours,
#   record this data, and then plot it. This particular activity was done in a group,
#   with Cole Andrews and Ben Spin. 
#   * Source: https://bitbucket.org/jayericks/me405_assignments/src/master/Lab4/
#
#   * Nucleo Main File : \ref main_0x04
#   * Temperature Sensor Driver : \ref mcp9808
#
#   * Image : https://ibb.co/x2S8PqK
#   * Ben Spin : https://spinb17.bitbucket.io/
#   * Cole Andrews: https://candrews09.bitbucket.io/
#   @image html CoreAmbientTemp.jpg "Figure 2: Temperature Sensor Data Plot" width = 500cm
#
#   @section sec_lab5 Lab 5: Feelin' Tipsy?
#   In this lab, we were responsible for building out the model for how our
#   table and ball system operate. This was definitely not an easy task, but
#   we will see that our model actually performs quite well in the end.
#   * Source: https://bitbucket.org/jayericks/me405_assignments/src/master/Lab5/
#   * Documentation: \ref Proj_calcs
#
#   @section sec_lab6 Lab 6: Simulation or Reality?
#   Now that our model was build, it was time to do simulations of how our 
#   model would respond under certain coniditons. This was all done using 
#   Simulink in MATLAB, and can be found in the Simulations page in the Term
#   Project documentation. This lab was done with my Term Project partner,
#   Matt Murray. The link to his bitbucket.io page can be found below. 
#   * These simulations can be found here: \ref Lab6_Project
#   * Source: https://bitbucket.org/jayericks/me405_assignments/src/master/Lab6/
#   * Matt Murray: https://mattmurray99.bitbucket.io/index.html
#
#   @section sec_lab7 Lab 7: Feeling Touchy
#   We defined two of our state variables above with regards to the position 
#   and velocity of the ball on the table. However, up to this point, we have
#   no way of actually measuring this information. In this weeks lab, we 
#   developed this capability using a resistive touch screen. A picture of 
#   the full hardware setup can be seen below in Figure 3. We first had to build
#   a driver for using the touchscreen, which would allow us to measure the X 
#   and Y positions of an object, as well as the "Z" position. The "Z" position
#   has quotes because (of course) there is no Z position on the screen. Instead, 
#   this is simply a boolean telling us if there is something touching the platform. 
#   Each of these measurements were first done individually, but then done all together
#   in a loop, which was the final driver \ref ResistiveTouch.
#
#   * Source: https://bitbucket.org/jayericks/me405_assignments/src/master/Lab7/
#   * Main Loop running on Nucleo : \ref main7
#   * Class Definition: \ref ResistiveTouch
#   * Speed for reading X,Y,and Z: 805 uS, Pin Assignments: 12
#   * Speed for reading X: ~ 268 uS, Pin Assignments: 5
#   * Speed for reading Y: ~ 268 uS, Pin Assignments: 5
#   * Speed for reading Z: ~ 268 uS, Pin Assignments: 5
#   @image html lab7_hardwaresetup.jpg "Hardware Setup" width = 250cm
#
#   @section sec_lab8 Lab 8 : Term Project Part I
#   This activity required that we build out the drivers for operating the
#   motors as well as reading the encoders. This lab was done in conjunction
#   with Matt Murray, whose bitbucket.io page can be found below. 
#   * Source: https://bitbucket.org/jayericks/me405_assignments/src/master/Lab8/
#   * Matt Murray: https://mattmurray99.bitbucket.io/index.html
#   * Encoders: \ref Encoder
#   * Motor Driver: \ref MotorDriver
#
#   @author Jonny Erickson
#
#   @date March 10 2021
#
#
#
#
#
#   @page final_proj_desc Final Project Description
#
#   @section sec_finalIntro Introduction
#   This page is dedicated as a description of the term project. The project was done alongside my lab partner,
#   Matt Murray. We also worked in conjunction on the development of the following pages of documentation.
#
#   @section sec_finalcalc Model Calculations
#   The project began with the need to model our system so that we may then design a controller for the system. These
#   calculations were first done by hand and then formatted with Latex. Please reference the Final Project Calcs page
#   to view the modeling of our system. The final result of our modeling resulted in the matrix form below:
#   @image html Lab5_pt4.PNG width=750cm
#
#   @section sec_finalsim Model Simulation
#   The next step to our project was to simulate the system. Before we could do this we had to first uncouple our M
#   matrix into a series of first order equations. This resulted in a very non-linear representation. The next step
#   was to use a jacobian matrix to linearize our M matrix as well as our U matrix. Evaluating these at the origin
#   resulted in our A and B matrices. Teh process of uncupling and linearizing can be seen in the Final Project Cals.
#   Page. Once the system was in a familiar form we then simulated the system in simulink using a set of initial
#   conditions. To view these results please see the Final Project Simulations page.
#
#   @section sec_EE EE513 Further Work
#   Matt Murray and I concurrently worked on a side project for EE513 while also working on this final project.
#   the EE project focused on topics such as uncoupling, linearization, observibility/controllability, and feedback
#   controllers. Through this project we were able to define and fine tune the feedback controller needed to control
#   the system. The work done in simulation for this project exceeds what was required for this class but is a good
#   reference to what was done to optimize the control. Please see the EE513 Controls page for reference.
#
#   @section sec_finalTP Touch Pad
#   The first of two key sensors in this project is the touch pad. This resistive touch screen is used to determine
#   the position of the ball. the driver for this sensor can be seen in Lab 7 and is documented
#   in \ref ResistiveTouch Teh driver was tuned by first determining the active region on the screen and measuring the
#   total distance along each axis. Then by measuring the ADC values read at each of these extreme points a conversion
#   factor for ADC points per centimeter was created for each axis. Then by subtracting the value at the origin the
#   deviated distance from the center could be measured. The Z measurement was used to determing if a press had been
#   made and was reported as a boolean
#
#   The key requirement for this driver was execution time. This is because the speed requirement for the whole system
#   must be fast enough so that the controller can correctly level the ball. The execution time for each of the methods
#   can be seen in the below:
#   * Speed for reading X,Y,and Z: 805 uS, Pin Assignments: 12
#   * Speed for reading X: ~ 268 uS, Pin Assignments: 5
#   * Speed for reading Y: ~ 268 uS, Pin Assignments: 5
#   * Speed for reading Z: ~ 268 uS, Pin Assignments: 5
#
#   @section sec_finalEnc Encoder
#   The second sensor used in this project is the encoder. This was accomplished in Lab 8 and the documentation can be
#   seen in \ref Encoder The Encoder is used to measure the current angle of the motor based off of the encoders start.
#   The driver for this sensor uses the already installed encoders on each of the motors. The encoders operate
#   by sending digital signals over two wires in quadrature. We use the built in hardware timers to incrementally count
#   up and down based off of these signals. To convert this to radians we do a similarity to the touch pad where we
#   create a scaling factor that converts timer counts to radians. To handle underflow the driver updates the current
#   position in small deltas and if said delta is very large the delta is reduced by the max counter count.
#   by frequently using the update() method we are able to keep constant track of the angle of the table.
#
#   @section sec_finalMot Motor
#   The final driver needed to implement the final project is the motor driver. Teh motor driver was completed in
#   Lab 8 and the documentation can be see in \ref MotorDriver The motor driver works as most motors do and operates
#   with PWM inputs. Depending on the pin used the direction goes CCW or CW. The motor driver class implemets methods
#   to enable and disable the motors as well as set the duty cycle. Furthermore, the motor driver uses an n-fault pin
#   to determine if there is an over current event in the case of a stuck finger or an unstable control system.
#
#   @section sec_finalCont Controller
#   Now that all of the physical sensors and actuators have been integrated with their respective drivers it is time
#   to design the controller. The first part of the controller is to define the feedback gain parameters K. K is a
#   1x4 column vector that is used in the feedback path to control the system. The values of K will be discussed in
#   the Final Project section. K is multiplied by the state variables to give negative feedback to the system. This
#   results in a value of torque needed on the motor. This torque is then converted to duty cycle and returned from the
#   class
#
#   @section sec_final Final Project
#   Finally that all the work above has been done it is time to put everything together. The list of included modules
#   are as follows:
#
#    \ref ResistiveTouch :     Measure X,Y,Z of the ball \n
#    \ref Encoder      :    Measure angle theta in the X and Y axies \n
#    \ref MotorDriver  :     Drive the motor, set duty, enable/disable \n
#    \ref Controller   :     Compute duty cycle needed to control the system \n
#
#   To design the final project we had to first decide on what kind of software architecture to use. First we could
#   use a linear model within a main page that utilizes functions to pull and compute our necessary metrics. Or we could
#   use a scheduling algorithm comprised of tasks to do the same thing. For our project we decided to use a linear
#   system due to the need to have fresh data each time we compute new duty cycles for our motors.
#
#   The next step in our design was to create functions that calculated the ball velocity and the angular velocity for
#   each axis. This was done by capturing the execution time to measure two values and calculate the velocity for
#   each metric.
#
#   The final part of the design was to determine the values for the K matrix. Using the simulation from the Final
#   Project Simulation page as well as the EE513 Controls page we decided to use an LQR controller that is tuned to
#   give acceptable overshoot and settling time. The theoretical values for the K matrix are as follows:
#                                      K = [-0.3211 -0.3552 -0.0939 -0.0436]
#
#   Using these K values we found that the model does not account for the friction of the table and our K values were
#   not sufficient in giving us the needed torque. To fix this we added an additional scaling factor to our conversion
#   from torque to duty cycle. Also we added a DC gain to our motor drivers of +25% that way at small duty cycles there
#   would be enough torque from the motors to slightly move the tables.
#
#   After accounting for friction we fond that our simulation results did not match the real world and we had to modify
#   the gains to properly control the system. By a settles of trial and error we landed on the following K values that
#   controlled the system to the best of our ability.
#                                       K = [.4164  -.2584  -.0390  -.00147]
#
#   @section sec_finalVid   Performance Video
#   To demonstrate the performance of our system we have recorded a short video explaining our process and results
#   Video Link: https://youtu.be/ZfK79FwN5YA
#
#   @page Proj_calcs Final Project Calculations
#
#   @section sec_calcsModel Model Calculations
#   The work below shows the development of the F and M matrices for Lab 5 and the Final Project. The document shown
#   below is pulled off the report from EE513 controls and can be seen in the respective page. The work will be the same
#   as in Matt Murray's documentation as we did the second pass together to make sure the math was correct.
#
#   @image html Calcs_pt1.PNG width=1000cm
#   @image html Calcs_pt2.PNG width=1000cm
#   @image html Calcs_pt3.PNG width=1000cm
#   @image html Calcs_pt4.PNG width=1000cm
#
#   @section sec_calcsLin System Linearization
#   The work below shows the uncoupling of the system and the linearization of the model
#
#   @image html Calcs_pt5.PNG width=1000cm
#   @image html Calcs_pt6.PNG width=1000cm
#
#   @page Lab6_Project Final Project Simulations
#
#   @section sim_OL Open Loop Simulations
#   Within this section there are simulation plots of the system in an open loop. The system is given a series
#   of initial conditions as described in the Lab 6 instructions. The open loop block diagram can be seen below
#
#   @image html Lab5_sec1_pt1.png "The above diagram shows the open loop simulation block diagram"
#
#
#   The first set of initial conditions is when the ball is at rest on the center of gravity of the plate(origin)
#   and there is no angle on the table.
#   @image html Lab6_sec1_pt2.png width=600cm
#   The plot above shows how the system will react with the initial conditions equal to the equilibrium point.
#    the flat line is expected as the system does not need to react.
#
#
#   The next set of initial conditions is when the ball is placed 5cm from the center of the table. As before
#   there is no tilt to the table.
#   @image html Lab6_sec1_pt3.png width=600cm
#   The plot above shows the ball's position constant at 5cm away from the center. The other parameters do not
#   change as there is no feedback on the system to drive the motors one way or another
#
#
#   The next set of initial condition is now where the ball is at the center of the table and the table is tilted
#   by 5 degrees. The results are shown below
#   @image html Lab6_sec1_pt4.png  width=650cm
#   The plot above shows the table at a 5deg tilt. We can expect to see the ball's velocity and position begin
#   to increase as the ball moves down the table.
#
#
#   The final set of initial conditions is when the table experiences a 1mNm/s impulse. This is equivalent to the
#   table experiencing a nudge creating an angular velocity to the table
#   @image html Lab6_sec1_pt5.png  width=600cm
#   The plot above shows how the system reacts when an impulse is applied to the angular velocity of the table.
#   with a constant angular velocity we can see the angle of the table increase liberally and the ball's position
#   and speed increase as well.
#
#   @section sec_simCL Closed Loop Simulations
#   within this section there are simulation plots of the system in a closed loop configuration. Using the same set
#   of initial conditions we are able to see the difference between the performance of closed loop vs open loop.
#   the block diagram is also given below. The feedback gain constants used are the ones given in class and are
#                                   K = [-0.05  -0.02  -0.3  -0.2]
#
#   @image html Lab6_sec2_pt1.png "The diagram above shows the system in a closed loop configuration." width=600cm
#
#
#   The first set of initial conditions is when the ball is at rest on the center of gravity of the plate(origin)
#   and there is no angle on the table.
#   @image html Lab6_sec2_pt2.png width=600cm
#   The plot above shows the system with initials at the equilibrium point. The results are expected as system does
#   not need to react in order to keep the ball centered
#
#
#   The next set of initial conditions is when the ball is placed 5cm from the center of the table. As before
#   there is no tilt to the table.
#   @image html Lab6_sec2_pt3.png width=600cm
#   The plot above shows how the system brings the ball into equilibrium. The ball's position gradually approaches
#   zero. The table angle is very small in order to achieve this.
#
#
#   The next set of initial condition is now where the ball is at the center of the table and the table is tilted
#   by 5 degrees. The results are shown below
#   @image html Lab6_sec2_pt4.png  width=600cm
#   The plot above shows the systems response to a tilt in the table. We can see the ball move away from center and
#   then the system react to bring the ball into equilibrium. We can see that the gain is not correct as the system
#   over corrects
#
#
#   The final set of initial conditions is when the table experiences a 1mNm/s impulse. This is equivalent to the
#   table experiencing a nudge creating an angular velocity to the table
#   @image html Lab6_sec2_pt5.png width=600cm
#   The plot above shows the response to the impulse in the table angular velocity. like 3c) we can see the ball's
#   position increase then correct back to center. However we see an over correct like before.
#
#
#   @page EE_proj EE513 Controls
#
#   @section sec_head Controls Analysis from EE513
#   This final section is the report we submitted in EE513, going in depth on
#   the control systems analysis for this course. We saw these two final projects
#   as a great way to dig deeper on this super interesting problem.
#
#   @image html EE513_1.PNG width=1000cm
#   @image html EE513_2.PNG width=1000cm
#   @image html EE513_3.PNG width=1000cm
#   @image html EE513_4.PNG width=1000cm
#   @image html EE513_5.PNG width=1000cm
#   @image html EE513_6.PNG width=1000cm
#   @image html EE513_7.PNG width=1000cm
#   @image html EE513_8.PNG width=1000cm
#   @image html EE513_9.PNG width=1000cm
#   @image html EE513_10.PNG width=1000cm
#   @image html EE513_11.PNG width=1000cm
#   @image html EE513_12.PNG width=1000cm
#   @image html EE513_13.PNG width=1000cm
#   @image html EE513_14.PNG width=1000cm
#   @image html EE513_15.PNG width=1000cm
#   @image html EE513_16.PNG width=1000cm
#   @image html EE513_17.PNG width=1000cm
#   @image html EE513_18.PNG width=1000cm
#   @image html EE513_19.PNG width=1000cm
#   @image html EE513_20.PNG width=1000cm
#   @image html EE513_21.PNG width=1000cm
#   @image html EE513_22.PNG width=1000cm
#   @image html EE513_23.PNG width=1000cm
#   @image html EE513_24.PNG width=1000cm
#   @image html EE513_25.PNG width=1000cm
#   @image html EE513_26.PNG width=1000cm
#   @image html EE513_27.PNG width=1000cm
#   @image html EE513_28.PNG width=1000cm
#
